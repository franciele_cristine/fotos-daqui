var map, infoWindow;
function initMap() {
  map = new google.maps.Map(document.getElementById('map'), {
    center: { lat: 50.720146, lng: -1.843295 },
    zoom: 10
  });
  infoWindow = new google.maps.InfoWindow;

  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function (position) {
      var pos = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };

      infoWindow.setPosition(pos);
      infoWindow.setContent('Location found.');
      infoWindow.open(map);
      map.setCenter(pos);
    }, function () {
      handleLocationError(true, infoWindow, map.getCenter());
    });
  } else {

    handleLocationError(false, infoWindow, map.getCenter());
  }
 
}

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
  infoWindow.setPosition(pos);
  infoWindow.setContent(browserHasGeolocation ?
    'Error: The Geolocation service failed.' :
    'Error: Your browser doesn\'t support geolocation.');
  infoWindow.open(map);
}

  fetch('https://shrouded-mountain-15003.herokuapp.com/https://flickr.com/services/rest/?api_key=e3866a46e97a0c8905a078c8958ead5b&format=json&nojsoncallback=1&method=flickr.photos.search&safe_search=1&per_page=100&lat=50.720146&lon=-1.843295&text=skyscraper')
  .then(data => data.json())
  .then(foto => percorrerFotos(foto.photos.photo))


let url = [];
var index = 0

function percorrerFotos(photoObj) {
  for (let i = 0; i < photoObj.length; i++) {
    url.push(constructImageURL(photoObj[i]))
  }

  setInterval(function urlchange() {
    {
      index += 1
      document.querySelector("#fotos").src = url[index]
      document.querySelector("#link").href = url[index]
      console.log(url)
      console.log(index)
    }

  }, 10000)

}




function constructImageURL(photoObj) {
  return ("https://farm" + photoObj.farm +
    ".staticflickr.com/" + photoObj.server +
    "/" + photoObj.id + "_" + photoObj.secret + ".jpg")
}
